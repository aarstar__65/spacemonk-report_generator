const fontFaces = `@font-face {
  font-family:'Proxima Nova';
  src: url("https://s3.ap-south-1.amazonaws.com/app.spacemonk.io/fonts/ProximaNova-Regular.otf") format("opentype");
}
@font-face 
{font-family: 'Proxima Nova';
font-weight: bold;
src: url("https://s3.ap-south-1.amazonaws.com/app.spacemonk.io/fonts/Proxima+Nova+Bold.otf") format("opentype");}`;

const templates = {
  HTMLStart: `<html>
  <head><style>
  ${fontFaces}
  body {font-family: "Proxima Nova";} 
  tr:nth-child(even) { background:#f5f5f5 } 
      th {padding:20;color:rgba(255, 255, 255, 1);background-color: rgba(208, 0, 0, 1)} 
      td {padding: 20px;color:#3f3e60;background-color: rgba(175, 175, 175 1); border-right: 1px solid #d5d5d5} 
      tr {page-break-inside:avoid; page-break-after:auto} 
      td {page-break-inside:avoid; page-break-after:auto} 
      table { overflow:hidden;border-spacing: 0;border:0px solid black;border-radius:20px;border-collapse: collapse;page-break-after:always !important;page-break-inside: avoid;} 
      </style></head><body>`,
  HTMLEnd: `</body>
  </html>`,
  weeklyReportTitle: function (
    type,
    startDate,
    endDate,
    company,
    location,
    logo
  ) {
    const wrpf = ` <div style="display:inline-block; margin-bottom:10px;">
    <span style="color:#000000;font-size:17px">Weekly Report</span>
    </div>`;
    const datePreFix = ` <div style="display:inline-block">
    <span style="color:#000000;font-size:17">${startDate} &nbsp;&nbsp; to &nbsp;&nbsp; ${endDate}</span>
    </div>`;
    const titles = {
      PPM: `<div>
    <span style="color:#3f3e60;font-size:22px"><b>${location}: PPM Workorder</b></span>
    </div>`,
      Breakdown: `<div>
    <span style="color:#3f3e60;font-size:22px"><b>${location}: Breakdown Workorder</b></span>
    </div>`,
      user: `<div>
    <span style="color:#3f3e60; font-size:22px"><b>${location}: Users</b></span>
    </div>`,
      asset: `<div>
    <span style="color:#3f3e60; font-size:22px"><b>${location}: Assets</b></span>
    </div>`,
      items: `<div>
    <span style="color:#3f3e60; font-size:22px"><b>${company}</b></span>
    </div>`,
    };
    const title = wrpf + titles[type] + datePreFix;
    return `<div style="margin-left:38px; margin-bottom:50px">
    <img align='right' src="${logo}">
            ${title}
          </div>`;
  },
  dailyReportTitle: function (companyName, type, name, date, surName, logo) {
    const drpf = (companyName) => ` <div style="display:inline-block;">
    <span style="color:#000000;font-size:17px;font-weight:bold">${companyName}</span>
    </div>`;
    const datePreFix = ` <div style="display:inline-block">
    <span style="color:#000000;font-size:17">${
      surName ? `${surName} (${date})` : `(${date})`
    }</span>
    </div>`;
    const titles = {
      generic: `<div>
    <span>${name}</span>
    </div>`,
    };
    const title =
      drpf(companyName) +
      `<div>
    <span>${name}</span>
    </div>` +
      datePreFix;
    return `<div style="margin-left:38px; margin-bottom:50px">
    <img align='right' src="${logo}" style="height:100px; width:200px; margin-top:-6px">
            ${title}
          </div>`;
  },
  tableEnd: `</tbody>
          </table>
          </div>`,
  PPMTableHead: `
          <table
            style="
              border: 1px solid black;
              margin-left: auto;
              margin-right: auto;
              width: 90%;
              page-break-after: always;
            ">
               <thead><tr>
                <th class="head" style="text-align: center;">
                 Date
                </th>
                <th class="head" style="text-align: center;">
                 Total PPM
                </th>
                <th class="head" style="text-align: center;">
                 Closed PPM
                </th>
                <th class="head" style="text-align: center;">
                 Time Taken
                </th>
              </tr></thead><tbody>`,
  companyItemTableHead: `
          <table
            style="
              border: 1px solid black;
              margin-left: auto;
              margin-right: auto;
              width: 90%;
              page-break-after: always;
            ">
               <thead><tr>
                <th class="head" style="text-align: center;">
                Location
                </th>
                <th class="head" style="text-align: center;">
                 PPM
                </th>
                <th class="head" style="text-align: center;">
                 Breakdown
                </th>
                <th class="head" style="text-align: center;">
                 Assets
                </th>
                <th class="head" style="text-align: center;">
                 Vendors
                </th>
              </tr></thead><tbody>`,
  BreakdownTableHead: `
          <table
            style="
              border: 1px solid black;
              margin-left: auto;
              margin-right: auto;
              width: 90%;
              page-break-after: always;
            ">
              <thead><tr>
                <th class="head" style="text-align: center;">
                 Date
                </th>
                <th class="head" style="text-align: center;">
                 Total Breakdown
                </th>
                <th class="head" style="text-align: center;">
                 Closed Breakdown
                </th>
              </tr></thead><tbody?`,
  userTableHead: `
          <table
            style="
              border: 1px solid black;
              margin-left: auto;
              margin-right: auto;
              width: 90%;
              page-break-after: always;
            ">
            <colgroup>
              <col width="100" />
              <col width="100" />
              <col width="100" />
              <col width="100" />
              <col width="100" />
            </colgroup>
            <thead><tr>
                <th class="head" rowspan="2" colspan="1" style="text-align: center;">
                  <div>
                   User
                  </div>
                </th>
                <th class="head" rowspan="1" colspan="2" style="text-align: center;">
                 PPM
                </th>
                <th class="head" rowspan="1" colspan="2" style="text-align: center;">
                 Breakdown
                </th>
              </tr>
              <tr>
                <th class="head" style="text-align: center;">
                  Total PPM
                </th>
                <th class="head" style="text-align: center;">
                  Closed PPM
                </th>
                <th class="head" style="text-align: center;">
                  Total Breakdown
                </th>
                <th class="head" style="text-align: center;">
                  <div>
                    <div>
                      Closed Breakdown
                    </div>
                  </div>
                </th>
              </tr></thead><tbody>`,
  assetTableHead: `
          <table
            style="
              border: 1px solid black;
              margin-left: auto;
              margin-right: auto;
              width: 90%;
            " >
            <colgroup>
              <col width="100" />
              <col width="100" />
              <col width="100" />
              <col width="100" />
              <col width="100" />
            </colgroup>
            <thead><tr>
                <th class="head" rowspan="2" colspan="1" style="text-align: center;">
                  <div>
                   Asset
                  </div>
                </th>
                <th class="head" rowspan="1" colspan="2" style="text-align: center;">
                 PPM
                </th>
                <th class="head" rowspan="1" colspan="2" style="text-align: center;">
                 Breakdown
                </th>
              </tr>
              <tr>
                <th class="head" style="text-align: center;">
                  Total PPM
                </th>
                <th class="head" style="text-align: center;">
                  Closed PPM
                </th>
                <th class="head" style="text-align: center;">
                  Total Breakdown
                </th>
                <th class="head" style="text-align: center;">
                  <div>
                    <div>
                      Closed Breakdown
                    </div>
                  </div>
                </th>
              </tr></thead><tbody>`,
  PPMRow: (rowIdentifier, data) => `<tr>
                <td style="text-align: center;">
                  ${rowIdentifier}
                </td>
                <td style="text-align: center;">
                  ${data.ppmtotal}
                </td>
                <td style="text-align: center;">
                  ${data.ppmclosed}
                </td>
                <td style="text-align: center;">
                  ${data.timeTaken}
                </td>
              </tr>`,
  companyItemTableRow: (rowIdentifier, data) => `<tr>
                <td style="text-align: center;">
                  ${rowIdentifier}
                </td>
                <td style="text-align: center;">
                  ${data.PPMs}
                </td>
                <td style="text-align: center;">
                  ${data.Breakdowns}
                </td>
                <td style="text-align: center;">
                  ${data.Assets}
                </td>
                <td style="text-align: center;">
                  ${data.Vendors}
                </td>
              </tr>`,
  BreakdownRow: (rowIdentifier, data) => `<tr>
                <td style="text-align: center;">
                  ${rowIdentifier}
                </td>
                <td style="text-align: center;">
                  ${data.breakdowntotal}
                </td>
                <td style="text-align: center;">
                  ${data.breakdownclosed}
                </td>
              </tr>`,
  userRow: (rowIdentifier, data) => `<tr>
                <td style="text-align: center;">
                  ${rowIdentifier}
                </td>
                <td style="text-align: center;">
                  ${data.ppmtotal}
                </td>
                <td style="text-align: center;">
                  ${data.ppmclosed}
                </td>
                <td style="text-align: center;">
                  ${data.breakdowntotal}
                </td>
                <td style="text-align: center;">
                  ${data.breakdownclosed}
                </td>
              </tr>`,
  assetRow: (rowIdentifier, data) => `<tr>
                <td style="text-align: center;">
                  ${rowIdentifier}
                </td>
                <td style="text-align: center;">
                  ${data.ppmtotal}
                </td>
                <td style="text-align: center;">
                  ${data.ppmclosed}
                </td>
                <td style="text-align: center;">
                  ${data.breakdowntotal}
                </td>
                <td style="text-align: center;">
                  ${data.breakdownclosed}
                </td>
              </tr>`,
  itemBlocks: (data, location, startDate, endDate, logo) => {
    return `<img align='right' src="${logo}" style="max-height:100px; max-width:200px;">
    <div style="display:inline-block; margin-left:38px; margin-bottom:50px">
    <div style="display:inline-block; margin-bottom:10px;">
    <span style="color:#000000;font-size:17px">Weekly Report</span>
    </div>
    <div>
    <span style="color:#3f3e60;font-size:22px"><b>${location}</b></span>
    </div>
    <span style="color:#000000;font-size:15px">${startDate} &nbsp;&nbsp; to &nbsp;&nbsp; ${endDate}</span>
    </div>
    <center><div
            style="
              page-break-after: always;
            "
          >
          <div>
          <div style="display:inline-block; text-align:center; border:1px solid rgba(125, 125, 125, 1);margin-top:5px;width:274.5px;margin:20px;border-radius:30px; padding-top:84px;padding-bottom:84px;">
          <span style="color:rgba(208, 0, 0, 1); font-size:40px;font-weight:bold">${data.PPMs}</span>
    <br />
    <br />
    <span style="color:#3f3e60;font-size:30px; font-weight:bold">PPM</span>
    </div>
    <div style="display:inline-block; text-align:center; border:1px solid rgba(125, 125, 125, 1);margin-top:5px;width:274.5px;margin:20px;border-radius:30px; padding-top:84px;padding-bottom:84px;">
     <span style="color:rgba(208, 0, 0, 1); font-size:40px;font-weight:bold">${data.Breakdowns}</span>
    <br />
    <br />
   <span style="color:#3f3e60;font-size:30px; font-weight:bold">Breakdown</span>
    </div>
    </div>
    <div>
          <div style="display:inline-block; text-align:center; border:1px solid rgba(125, 125, 125, 1);margin-top:5px;width:274.5px;margin:20px;border-radius:30px; padding-top:84px;padding-bottom:84px;">
          <span style="color:rgba(208, 0, 0, 1); font-size:40px;font-weight:bold">${data.Assets}</span>
    <br />
    <br />
    <span style="color:#3f3e60;font-size:30px; font-weight:bold">Assets</span>
    </div>
    <div style="display:inline-block; text-align:center; border:1px solid rgba(125, 125, 125, 1);margin-top:5px;width:274.5px;margin:20px;border-radius:30px; padding-top:84px;padding-bottom:84px;">
     <span style="color:rgba(208, 0, 0, 1); font-size:40px;font-weight:bold">${data.Vendors}</span>
    <br />
    <br />
   <span style="color:#3f3e60;font-size:30px; font-weight:bold">Vendors</span>
    </div>
    </div>
          </div></center>`;
  },
  SummaryTableHead: (fields) => `
          <table
            style="
              border: 1px solid black;
              margin-left: auto;
              margin-right: auto;
              width: 90%;
              page-break-after: always;
            "
          >
           
               <thead><tr>
               <th class="head" style="text-align: center;">
               KPIs
                </th>
                ${fields.reduce(
                  (header, currentColumn) =>
                    ` ${header}<th class="head" style="text-align: center;">
                    ${currentColumn}
                  </th>`,
                  ""
                )}
              </tr></thead><tbody>`,
  summaryRow: (rowIdentifier, data) => `<tr>
                <td style="text-align: center;">
                  ${rowIdentifier}
                </td>
                <td style="text-align: center;">
                  ${data["Total"] !== undefined ? data["Total"] : "-"}
                </td>
                <td style="text-align: center;">
                  ${
                    data["To be Verfied"] !== undefined
                      ? data["To be Verfied"]
                      : "-"
                  }
                </td>
                <td style="text-align: center;">
                  ${data["Done"] !== undefined ? data["Done"] : "-"}
                </td>
              </tr>`,
  genericReportTableHead: (data) =>
    `
          <table
            style="
              border: 1px solid black;
              margin-left: auto;
              margin-right: auto;
              width: 90%;
              page-break-after: always;
            "
          >
           
               <thead><tr>
                <th class="head" style="text-align: center;">
                 S. No.
                </th>
                ${data.reduce(
                  (head, currentField) =>
                    `${head} <th class="head" style="text-align: center;">
                    ${currentField}
                  </th>`,
                  ""
                )}
              </tr></thead><tbody>`,
  genericRow: (rowIdentifier, data, fields) => `<tr>
                <td style="text-align: center;">
                  ${rowIdentifier}
                </td>
                ${fields.reduce(
                  (row, currentField) => `
                ${row}<td style="text-align: center;">
                  ${data[currentField]}
                </td>`,
                  ""
                )}
              </tr>`,
};

module.exports = templates;

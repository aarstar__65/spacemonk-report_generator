var fs = require("fs");
var pdf = require("html-pdf");
var path = require("path");
const data = require("./testData");
const templates = require("./templates");

var options = {
  format: "Letter",
  header: {
    height: "15mm",
    contents: "",
  },
  footer: {
    height: "28mm",
    contents: "",
  },
};

const footer = `<div
  id="pageFooter"
  style="
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: flex-end;
    border-bottom: 1px solid rgba(150, 150, 150, 1);
  "
>
  <center><img src="https://s3.ap-south-1.amazonaws.com/images.spacemonk.io/images/report/powered+by.png" /></center>
</div>
`;

function buildItemsTable(type, data, location, startDate, endDate) {
  if (data) {
    let report = templates.companyItemTableHead;
    let company = "";
    let startDate = "";
    let endDate = "";

    data.forEach((dataSet) => {
      report =
        report + templates.companyItemTableRow(dataSet.location, dataSet.items);
      company = dataSet.company;
      startDate = dataSet.startDate;
      endDate = dataSet.endDate;
    });
    return (
      templates.weeklyReportTitle(
        "items",
        startDate,
        endDate,
        company,
        undefined,
        data[0].logo
      ) +
      report +
      templates.tableEnd
    );
  }
}

function buildWorkOrderReport(
  data,
  startDate,
  endDate,
  company,
  location,
  logo
) {
  if (data) {
    let PPMRows = "";
    for (let key in data) {
      PPMRows += templates.PPMRow(key, data[key], "week", "PPM");
    }
    let BreakdownRows = "";
    for (let key in data) {
      BreakdownRows += templates.BreakdownRow(
        key,
        data[key],
        "week",
        "Breakdown"
      );
    }
    return (
      templates.weeklyReportTitle(
        "PPM",
        startDate,
        endDate,
        company,
        location,
        logo
      ) +
      templates.PPMTableHead +
      PPMRows +
      templates.tableEnd +
      templates.weeklyReportTitle(
        "Breakdown",
        startDate,
        endDate,
        company,
        location,
        logo
      ) +
      templates.BreakdownTableHead +
      BreakdownRows +
      templates.tableEnd
    );
  }
}

function buildUserReport(
  type,
  data,
  startDate,
  endDate,
  company,
  location,
  logo
) {
  if (data) {
    let rows = "";
    for (let key in data) {
      rows += templates.userRow(key, data[key], type);
    }
    return (
      templates.weeklyReportTitle(
        type,
        startDate,
        endDate,
        company,
        location,
        logo
      ) +
      templates.userTableHead +
      rows +
      templates.tableEnd
    );
  }
}

function buildAssetReport(
  type,
  data,
  startDate,
  endDate,
  company,
  location,
  logo
) {
  if (data) {
    let rows = "";
    for (let key in data) {
      rows += templates.assetRow(key, data[key], type);
    }
    return (
      templates.weeklyReportTitle(
        type,
        startDate,
        endDate,
        company,
        location,
        logo
      ) +
      templates.assetTableHead +
      rows +
      templates.tableEnd
    );
  }
}

function buildItemsBlock(type, data, location, startDate, endDate, logo) {
  if (data) {
    return templates.itemBlocks(data, location, startDate, endDate, logo);
  }
}
1;

function buildCollectiveWeeklyReport(data) {
  let report = "";
  report =
    report +
    buildItemsTable(
      "items",
      data,
      data.location,
      data.startDate,
      data.endDate,
      data.logo
    );
  data.forEach((dataSet) => {
    report =
      report +
      buildWorkOrderReport(
        dataSet.week,
        dataSet.startDate,
        dataSet.endDate,
        dataSet.company,
        dataSet.location,
        dataSet.logo
      ) +
      buildUserReport(
        "user",
        dataSet.user,
        dataSet.startDate,
        dataSet.endDate,
        dataSet.company,
        dataSet.location,
        dataSet.logo
      ) +
      buildAssetReport(
        "asset",
        dataSet.asset,
        dataSet.startDate,
        dataSet.endDate,
        dataSet.company,
        dataSet.location,
        dataSet.logo
      );
  });

  const html = templates.HTMLStart + report + templates.HTMLEnd + footer;

  pdf.create(html, options).toFile("./report.pdf", function (err, res) {
    if (err) return console.log(err);
    console.log(res);
  });

  fs.writeFileSync(`report.html`, html);

  return 0;
}

// buildCollectiveWeeklyReport(data);

function buildSingleWeeklyReport(data) {
  let report = "";
  const itemsTable = buildItemsBlock(
    "items",
    data.items,
    data.location,
    data.startDate,
    data.endDate,
    data.logo
  );
  const weeklyReport = buildWorkOrderReport(
    data.week,
    data.startDate,
    data.endDate,
    data.company,
    data.location,
    data.logo
  );
  const userReport = buildUserReport(
    "user",
    data.user,
    data.startDate,
    data.endDate,
    data.company,
    data.location,
    data.logo
  );
  const assetReport = buildAssetReport(
    "asset",
    data.asset,
    data.startDate,
    data.endDate,
    data.company,
    data.location,
    data.logo
  );
  report =
    templates.HTMLStart +
    itemsTable +
    weeklyReport +
    userReport +
    assetReport +
    templates.HTMLEnd;

  const html = templates.HTMLStart + report + templates.HTMLEnd + footer;

  pdf.create(html, options).toFile("./report.pdf", function (err, res) {
    if (err) return console.log(err);
    console.log(res);
  });

  fs.writeFileSync(`report.html`, html);

  return 0;
}

// buildSingleWeeklyReport(data);

function buildSummaryTable(company, type, data, date, engagementLevel, logo) {
  if (data) {
    let rows = "";
    for (let key in data) {
      rows += templates.summaryRow(key, data[key]);
    }
    return (
      templates.dailyReportTitle(
        company,
        "generic",
        "Daily Report",
        date,
        undefined,
        logo
      ) +
      ` <center><div style="display:inline-block; margin-bottom:50px;margin-left:38px;margin-top:60px;text-align:center;font-weight:bold"><center><span >User engagement Level: </span><span > ${engagementLevel}</span></center></div></center>` +
      templates.SummaryTableHead(Object.keys(data[Object.keys(data)[0]])) +
      rows +
      templates.tableEnd
    );
  }
}

function reportTable(
  company,
  reportIdentifier,
  tableIdentifier,
  data,
  date,
  logo
) {
  if (data) {
    let rows = "";
    for (let key in data) {
      rows += templates.genericRow(
        parseInt(key) + 1,
        data[key],
        Object.keys(data[Object.keys(data)[0]])
      );
    }
    return (
      templates.dailyReportTitle(
        company,
        "generic",
        reportIdentifier,
        date,
        tableIdentifier,
        logo
      ) +
      templates.genericReportTableHead(
        Object.keys(data[Object.keys(data)[0]])
      ) +
      rows +
      templates.tableEnd
    );
  }
}

function typeReport(company, reportIdentifier, data, date, logo) {
  if (data) {
    let tables = "";
    for (let key in data) {
      tables += reportTable(
        company,
        reportIdentifier,
        key,
        data[key],
        date,
        logo
      );
    }
    return tables;
  }
}

function buildDailyReport(data) {
  const commonFields = [
    "location",
    "company",
    "logo",
    "date",
    "User Engagement Level",
    "Summary",
  ];
  const tableData = Object.keys(data).filter(
    (key) => !commonFields.includes(key)
  );
  let report = "";
  report =
    buildSummaryTable(
      data.company,
      "Summary",
      data.Summary,
      data.date,
      data["User Engagement Level"],
      data.logo
    ) +
    tableData.reduce((finalReport, currentTable) => {
      finalReport =
        finalReport +
        typeReport(
          data.company,
          currentTable,
          data[currentTable],
          data.date,
          data.logo
        );
      return finalReport;
    }, "");

  const html = templates.HTMLStart + report + templates.HTMLEnd + footer;

  pdf.create(html, options).toFile("./report.pdf", function (err, res) {
    if (err) return console.log(err);
    console.log(res);
  });

  fs.writeFileSync(`report.html`, html);

  return 0;
}

buildDailyReport(data);
